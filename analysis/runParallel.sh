for f in `ls $1`; do
    python analysis/analysis.py -i $1$f &
done
wait
rm -f `basename $1`/merged.root
hadd `basename $1`/merged.root `basename $1`/*
python analysis/plot.py -i `basename $1`/merged.root      -o allPlotsInc.pdf


for f in `ls $1`; do
    python analysis/analysis.py -i $1$f -d 0 -u 0.6 -s _eta1&
done
wait
rm -f `basename $1`_eta1/merged.root
hadd `basename $1`_eta1/merged.root `basename $1`_eta1/*
python analysis/plot.py -i `basename $1`_eta1/merged.root -o allPlotsEta1.pdf


for f in `ls $1`; do
    python analysis/analysis.py -i $1$f -d 0.6 -u 1.37 -s _eta2&
done
wait
rm -f `basename $1`_eta2/merged.root
hadd `basename $1`_eta2/merged.root `basename $1`_eta2/*
python analysis/plot.py -i `basename $1`_eta2/merged.root -o allPlotsEta2.pdf


for f in `ls $1`; do
    python analysis/analysis.py -i $1$f -d 1.37 -u 1.52 -s _eta3&
done
wait
rm -f `basename $1`_eta3/merged.root
hadd `basename $1`_eta3/merged.root `basename $1`_eta3/*
python analysis/plot.py -i `basename $1`_eta3/merged.root -o allPlotsEta3.pdf


for f in `ls $1`; do
    python analysis/analysis.py -i $1$f -d 1.52 -u 2.2 -s _eta4&
done
wait
rm -f `basename $1`_eta4/merged.root
hadd `basename $1`_eta4/merged.root `basename $1`_eta4/*
python analysis/plot.py -i `basename $1`_eta4/merged.root -o allPlotsEta4.pdf


for f in `ls $1`; do
    python analysis/analysis.py -i $1$f -d 2.2 -u 2.5 -s _eta5&
done
wait
rm -f `basename $1`_eta5/merged.root
hadd `basename $1`_eta5/merged.root `basename $1`_eta5/*
python analysis/plot.py -i `basename $1`_eta5/merged.root -o allPlotsEta5.pdf
