import sys
import inspect

import subprocess

import numpy as np


def analyse(file,eta1=-1,eta2=-1,suffix=""):
  import ROOT

  string = "mkdir -p "+(file.split("/")[-2])+suffix
  p = subprocess.Popen([string],shell=True,stdout=subprocess.PIPE)

  print "input file: ",file
  print file.split("/")[-1]
  print "reading TTree 'tree'"
  tfile = ROOT.TFile(file,"READ")
  tree  = tfile.Get("tree")
  NEVENTS = tree.GetEntries()
  print "number of entries: ", NEVENTS

  fName = file.split("/")[-1]

  # histograms
  from histograms import histos
  from histograms import variables

  # make data structure for branches
  import branches
  event,addVars = branches.initBranches(tree)

  # print addVars

  # event loop
  for i in range( tree.GetEntries() ):
    if i%10000 == 0:
      print fName," ",i,"/",NEVENTS," "," %.2f" % (float(i)/NEVENTS*100),"%\r"

    # read branches
    tree.GetEntry(i)
    
    # fill histograms
    for ele in range(event["el_pt"].size()):
      elePDGid = -11*event["el_charge"].at(ele)

      # electron must originate from a prompt electron gun electron
      if not (event["el_FirstEgMotherTyp"].at(ele) == 2 and event["el_FirstEgMotherOrigin"].at(ele) == 1):
        continue

      # it is good to define a working point..
      if not event["el_isLooseAndBLayerLLH_Smooth_v11"].at(ele):
        continue

      if not (eta2 > abs(event["el_etas2"].at(ele)) > eta1) and not (eta1==-1):
        continue

      for var,typ in variables:
        if var[0:3] != "el_":
          continue
        # prompt
        if   event["el_type"].at(ele) == 2 and event["el_origin"].at(ele) == 1 and event["el_firstEgMotherPdgId"].at(ele) == elePDGid:
          histos["prompt"][var].Fill(event[var].at(ele))
        # charge-flip type 2
        elif event["el_type"].at(ele) == 2 and event["el_origin"].at(ele) == 1 and event["el_firstEgMotherPdgId"].at(ele) != elePDGid:
          histos["type2"][var].Fill(event[var].at(ele))
        # brem
        elif event["el_type"].at(ele) == 4 and event["el_origin"].at(ele) == 5 and event["el_firstEgMotherPdgId"].at(ele) == elePDGid:
          histos["brem"][var].Fill(event[var].at(ele))
          # charge-flip type 4
        elif event["el_type"].at(ele) == 4 and event["el_origin"].at(ele) == 5 and event["el_firstEgMotherPdgId"].at(ele) != elePDGid:
          histos["type4"][var].Fill(event[var].at(ele))

      # # prompt
      # if   event["el_type"].at(ele) == 2 and event["el_origin"].at(ele) == 1 and event["el_firstEgMotherPdgId"].at(ele) == elePDGid:
      #   histos["prompt"]["cd0"].Fill(event["el_trackd0pvunbiased"].at(ele)*event["el_charge"].at(ele))
      # # charge-flip type 2
      # elif event["el_type"].at(ele) == 2 and event["el_origin"].at(ele) == 1 and event["el_firstEgMotherPdgId"].at(ele) != elePDGid:
      #   histos["type2"]["cd0"].Fill(event["el_trackd0pvunbiased"].at(ele)*event["el_charge"].at(ele))
      # # brem
      # elif event["el_type"].at(ele) == 4 and event["el_origin"].at(ele) == 5 and event["el_firstEgMotherPdgId"].at(ele) == elePDGid:
      #   histos["brem"]["cd0"].Fill(event["el_trackd0pvunbiased"].at(ele)*event["el_charge"].at(ele))
      #   # charge-flip type 4
      # elif event["el_type"].at(ele) == 4 and event["el_origin"].at(ele) == 5 and event["el_firstEgMotherPdgId"].at(ele) != elePDGid:
      #   histos["type4"]["cd0"].Fill(event["el_trackd0pvunbiased"].at(ele)*event["el_charge"].at(ele))
      
      # prompt
      el_deltaphiFromLM = event["el_deltaphiFromLM"].at(ele)
      track_charge = event["el_charge"].at(ele)
      track_x = event["el_xTrack"].at(ele)
      track_y = event["el_yTrack"].at(ele)
      bs_x = event["el_xBeamspot"].at(ele)
      bs_y = event["el_yBeamspot"].at(ele)
      track_phi = np.arctan( (track_y - bs_y) / (track_x - bs_x) )
      el_EGCluster_phimax1 = event["el_EGCluster_phimax1"].at(ele)
      el_EGCluster_phimax2 = event["el_EGCluster_phimax2"].at(ele)
      if i%10000 == 0:
        print "track_x: ",track_x
        print "track_y: ",track_y
        print "track_phi: ",track_phi
        print "(track_y - bs_y) / (track_x - bs_x): ",(track_y - bs_y) / (track_x - bs_x)
        print "el_EGCluster_phimax1: ",el_EGCluster_phimax1
        print "el_EGCluster_phimax2: ",el_EGCluster_phimax2
      if   event["el_type"].at(ele) == 2 and event["el_origin"].at(ele) == 1 and event["el_firstEgMotherPdgId"].at(ele) == elePDGid:
        histos["prompt"]["deltaPhiTrackCluster1"].Fill((track_phi-el_EGCluster_phimax1))
        histos["prompt"]["deltaPhiTrackCluster2"].Fill((track_phi-el_EGCluster_phimax2))
        histos["prompt"]["deltaPhiTrackCluster1_times_charge"].Fill((track_phi-el_EGCluster_phimax1)*track_charge)
        histos["prompt"]["deltaPhiTrackCluster2_times_charge"].Fill((track_phi-el_EGCluster_phimax2)*track_charge)
        histos["prompt"]["deltaphiFromLM_times_charge"].Fill(el_deltaphiFromLM*track_charge)
      # charge-flip type 2
      elif event["el_type"].at(ele) == 2 and event["el_origin"].at(ele) == 1 and event["el_firstEgMotherPdgId"].at(ele) != elePDGid:
        histos["type2"]["deltaPhiTrackCluster1"].Fill((track_phi-el_EGCluster_phimax1))
        histos["type2"]["deltaPhiTrackCluster2"].Fill((track_phi-el_EGCluster_phimax2))
        histos["type2"]["deltaPhiTrackCluster1_times_charge"].Fill((track_phi-el_EGCluster_phimax1)*track_charge)
        histos["type2"]["deltaPhiTrackCluster2_times_charge"].Fill((track_phi-el_EGCluster_phimax2)*track_charge)
        histos["type2"]["deltaphiFromLM_times_charge"].Fill(el_deltaphiFromLM*track_charge)
      # brem
      elif event["el_type"].at(ele) == 4 and event["el_origin"].at(ele) == 5 and event["el_firstEgMotherPdgId"].at(ele) == elePDGid:
        histos["brem"]["deltaPhiTrackCluster1"].Fill((track_phi-el_EGCluster_phimax1))
        histos["brem"]["deltaPhiTrackCluster2"].Fill((track_phi-el_EGCluster_phimax2))
        histos["brem"]["deltaPhiTrackCluster1_times_charge"].Fill((track_phi-el_EGCluster_phimax1)*track_charge)
        histos["brem"]["deltaPhiTrackCluster2_times_charge"].Fill((track_phi-el_EGCluster_phimax2)*track_charge)
        histos["brem"]["deltaphiFromLM_times_charge"].Fill(el_deltaphiFromLM*track_charge)
        # charge-flip type 4
      elif event["el_type"].at(ele) == 4 and event["el_origin"].at(ele) == 5 and event["el_firstEgMotherPdgId"].at(ele) != elePDGid:
        histos["type4"]["deltaPhiTrackCluster1"].Fill((track_phi-el_EGCluster_phimax1))
        histos["type4"]["deltaPhiTrackCluster2"].Fill((track_phi-el_EGCluster_phimax2))
        histos["type4"]["deltaPhiTrackCluster1_times_charge"].Fill((track_phi-el_EGCluster_phimax1)*track_charge)
        histos["type4"]["deltaPhiTrackCluster2_times_charge"].Fill((track_phi-el_EGCluster_phimax2)*track_charge)
        histos["type4"]["deltaphiFromLM_times_charge"].Fill(el_deltaphiFromLM*track_charge)


  f = ROOT.TFile((file.split("/")[-2])+suffix+"/"+(fName),"recreate")
  for var,typ in variables:
    f.mkdir(var+"/")
    f.cd(var)
    histos["prompt"][var].Write()
    histos["type2" ][var].Write()
    histos["brem"  ][var].Write()
    histos["type4" ][var].Write()
  f.Close()
  # sys.exit(0)