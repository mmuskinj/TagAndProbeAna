import ROOT

def initBranches(tree):
  # variables
  variables = [
    ["el_isLooseAndBLayerLLH_Smooth_v11", "std::vector<int>"],
    ["el_firstEgMotherPdgId", "std::vector<int>"],
    ["el_FirstEgMotherTyp", "std::vector<int>"],
    ["el_FirstEgMotherOrigin", "std::vector<int>"],
    ["el_type", "std::vector<int>"],
    ["el_origin", "std::vector<int>"],
    ["el_charge", "std::vector<float>"],
    ["el_pt", "std::vector<float>"],
    ["el_phi", "std::vector<float>"],
    ["el_etas2", "std::vector<float>"],
    ["el_EOverP", "std::vector<float>"],
    ["el_trackd0pvunbiased", "std::vector<float>"],
    ["el_d0significance", "std::vector<float>"],
    ["el_z0sinTheta", "std::vector<float>"],
    ["el_nTrackParticles", "std::vector<int>"],
    ["el_AmbiguityBit", "std::vector<int>"],
    ["el_nSCTHits", "std::vector<int>"],
    ["el_nPixHits", "std::vector<int>"],
    ["el_nBLHits", "std::vector<int>"],
    ["el_numberSC", "std::vector<int>"],
    ["el_SC0_Moment_AVG_LAR_Q",  "std::vector<float>"],
    ["el_SC0_Moment_AVG_TILE_Q",  "std::vector<float>"],
    ["el_SC0_Moment_BADLARQ_FRAC",  "std::vector<float>"],
    ["el_SC0_Moment_CENTER_LAMBDA",  "std::vector<float>"],
    ["el_SC0_Moment_CENTER_MAG",  "std::vector<float>"],
    ["el_SC0_Moment_EM_PROBABILITY",  "std::vector<float>"],
    ["el_SC0_Moment_ENG_BAD_CELLS",  "std::vector<float>"],
    ["el_SC0_Moment_ENG_POS",  "std::vector<float>"],
    ["el_SC0_Moment_ISOLATION",  "std::vector<float>"],
    ["el_SC0_Moment_N_BAD_CELLS",  "std::vector<float>"],
    ["el_SC0_Moment_SECOND_LAMBDA",  "std::vector<float>"],
    ["el_SC0_Moment_SECOND_R",  "std::vector<float>"],
    ["el_SCTWeightedCharge",  "std::vector<float>"],
    ["el_SCTWeightedCharge_2Alt",  "std::vector<float>"],
    ["el_E233", "std::vector<float>"],
    ["el_E237", "std::vector<float>"],
    ["el_E277", "std::vector<float>"],
    ["el_Ethad", "std::vector<float>"],
    ["el_Ethad1", "std::vector<float>"],
    ["el_f1", "std::vector<float>"],
    ["el_f3", "std::vector<float>"],
    ["el_weta1", "std::vector<float>"],
    ["el_weta2", "std::vector<float>"],
    ["el_DeltaE", "std::vector<float>"],
    ["el_wstot", "std::vector<float>"],
    ["el_Emins1", "std::vector<float>"],
    ["el_emaxs1", "std::vector<float>"],
    ["el_Emax2", "std::vector<float>"],
    ["el_fracs1", "std::vector<float>"],
    ["el_reta", "std::vector<float>"],
    ["el_rhad0", "std::vector<float>"],
    ["el_rhad1", "std::vector<float>"],
    ["el_rhad", "std::vector<float>"],
    ["el_rphi", "std::vector<float>"],
    ["el_eratio", "std::vector<float>"],
    ["el_e011", "std::vector<float>"],
    ["el_e033", "std::vector<float>"],
    ["el_e132", "std::vector<float>"],
    ["el_e1152", "std::vector<float>"],
    ["el_ehad1", "std::vector<float>"],
    ["el_f1core", "std::vector<float>"],
    ["el_f3core", "std::vector<float>"],
    ["el_e235", "std::vector<float>"],
    ["el_e255", "std::vector<float>"],
    ["el_e333", "std::vector<float>"],
    ["el_e335", "std::vector<float>"],
    ["el_e337", "std::vector<float>"],
    ["el_e377", "std::vector<float>"],
    ["el_widths1", "std::vector<float>"],
    ["el_widths2", "std::vector<float>"],
    ["el_poscs1", "std::vector<float>"],
    ["el_poscs2", "std::vector<float>"],
    ["el_asy1", "std::vector<float>"],
    ["el_pos", "std::vector<float>"],
    ["el_pos7", "std::vector<float>"],
    ["el_barys1", "std::vector<float>"],
    ["el_r33over37allcalo", "std::vector<float>"],
    ["el_ecore", "std::vector<float>"],
    ["el_deltaphiFromLM", "std::vector<float>"],
    ["el_DeltaPOverP", "std::vector<float>"],
    ["el_deltaphi1", "std::vector<float>"],
    ["el_deltaphi2", "std::vector<float>"],
    ["el_deltaphiRescaled", "std::vector<float>"],
    ["el_qoverpsignificance", "std::vector<float>"],
    ["el_tracksigd0pvunbiased", "std::vector<float>"],
    ["el_Chi2oftrackmatch", "std::vector<float>"],
    ["el_eProbabilityHT", "std::vector<float>"],
    ["el_TRTHighTOutliersRatio", "std::vector<float>"],
    ["el_pTErr", "std::vector<float>"],
    ["el_CFT", "std::vector<float>"],
    ["el_EGCluster_etaBE1", "std::vector<float>"],
    ["el_EGCluster_etaBE2", "std::vector<float>"],
    ["el_EGCluster_phiBE1", "std::vector<float>"],
    ["el_EGCluster_phiBE2", "std::vector<float>"],
    ["el_EGCluster_energyBE1", "std::vector<float>"],
    ["el_EGCluster_energyBE2", "std::vector<float>"],
    ["el_EGCluster_energy_max1", "std::vector<float>"],
    ["el_EGCluster_energy_max2", "std::vector<float>"],
    ["el_EGCluster_etamax1", "std::vector<float>"],
    ["el_EGCluster_etamax2", "std::vector<float>"],
    ["el_EGCluster_phimax1", "std::vector<float>"],
    ["el_EGCluster_phimax2", "std::vector<float>"],
    # ["el_EGCluster_phisize1", "std::vector<float>"],
    # ["el_EGCluster_phisize2", "std::vector<float>"],
    # ["el_EGCluster_etasize1", "std::vector<float>"],
    # ["el_EGCluster_etasize2", "std::vector<float>"],
    ["el_xTrack", "std::vector<float>"],
    ["el_yTrack", "std::vector<float>"],
    ["el_xBeamspot", "std::vector<float>"],
    ["el_yBeamspot", "std::vector<float>"],
  ]

  allVars = [x.GetName() for x in tree.GetListOfBranches()]
  addVars = []
  for var in allVars:
    if var[0:3] == "el_" and var not in [x for x,y in variables]+["el_originbkg"]:
        addVars += [var]

  string = '''struct Event {
'''
  for var in variables:
    string += var[1]+" *"+var[0]+" = new "+var[1]+";\n"
  for var in addVars:
    string += "std::vector<float> *"+var+" = new std::vector<float>;\n"
  string += '''};'''


  # print string
  ROOT.gROOT.ProcessLine( string )

  from ROOT import Event
  event = Event()

  # set address
  for var in variables:
    tree.SetBranchAddress(var[0],ROOT.AddressOf(event,var[0]))

  varMap = dict()
  for name,typ in variables:
    exec("varMap['"+name+"'] = event."+name)
  for name in addVars:
    exec("varMap['"+name+"'] = event."+name)

  return varMap,addVars