from optparse import OptionParser

import os
import re

import subprocess
import multiprocessing as mp

import EgammaCore.EgammaCore as EgammaCore

# ROOT.gROOT.Macro( os.path.expanduser( '~/RootUtils/rootlogon.c' ) )
# ROOT.gROOT.SetBatch(True)
# ROOT.SetAtlasStyle()

#-----------------
# input
#-----------------
parser = OptionParser()
parser.add_option('-i', '--infile', dest='infile',
                  help='input file',metavar='INFILE',default='/ceph/grid/home/atlas/miham/TandPNtuples/v3/user.mmuskinj.423000.ParticleGun_single_electron_egammaET.e3566_s3113_r9388_r9315.v3_SkimmedNtuple.root/user.mmuskinj.12156488._000002.SkimmedNtuple.root')
parser.add_option('-d', '--down', dest='down',
                  help='down',metavar='down',default="-1")
parser.add_option('-u', '--up', dest='up',
                  help='up',metavar='up',default="-1")
parser.add_option('-s', '--suffix', dest='suffix',
                  help='suffix',metavar='suffix',default="")
parser.add_option('-t', '--type', dest='type',
                  help='file type',metavar='TYPE',default="EgammaCore")

(options, args) = parser.parse_args()

INPUT = options.infile
if INPUT[:1] != "/":
  INPUT += "/"

def analyse(file):
  if options.type == "EgammaCore":
    EgammaCore.analyse(file,float(options.down),float(options.up),options.suffix)


def main():
  if os.path.isfile(INPUT):
    analyse(INPUT)
  if os.path.isdir(INPUT):
    allFiles = [f for f in os.listdir(INPUT) if os.path.isfile(os.path.join(INPUT, f))]
    doneFiles = [f for f in allFiles if "part" not in f]

    num_workers = mp.cpu_count()
    print "===== CPU count: ", num_workers," ====="  

    pool = mp.Pool(2)
    pool.daemon = True
    for f in doneFiles:
      print INPUT+"/"+f
      res = pool.apply_async(analyse, args = [str(INPUT+f)] )

    pool.close()
    pool.join()


if __name__=='__main__': main()