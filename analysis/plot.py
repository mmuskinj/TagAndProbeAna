import ROOT

from optparse import OptionParser

import os
import re

ROOT.gROOT.Macro( os.path.expanduser( '~/RootUtils/rootlogon.c' ) )
ROOT.gROOT.SetBatch(True)
ROOT.SetAtlasStyle()


#-----------------
# input
#-----------------
parser = OptionParser()
parser.add_option('-i', '--infile', dest='infile',
                  help='input file',metavar='INFILE',default="infile.root")
parser.add_option('-o', '--outfile', dest='outfile',
                  help='outfile file',metavar='outfile',default="outfile.root")

(options, args) = parser.parse_args()

f = ROOT.TFile(options.infile,"READ") if options.infile!=None else DEFAULTFILE

color = [ROOT.kBlack,ROOT.kRed,ROOT.kBlue,ROOT.kGreen]
typ = ["prompt","flip type 2","brem","flip type 4"]

tempHist1 = ROOT.TH1F();
tempHist2 = ROOT.TH1F();
tempHist3 = ROOT.TH1F();
tempHist4 = ROOT.TH1F();
tempHist2.SetLineColor(ROOT.kRed);
tempHist3.SetLineColor(ROOT.kBlue);
tempHist4.SetLineColor(ROOT.kGreen);

def main():

  for i,key in enumerate(f.GetListOfKeys()):
    name = key.GetName()
    histos = [
    f.Get(name+"/"+name+"_prompt"),
    f.Get(name+"/"+name+"_type2"),
    f.Get(name+"/"+name+"_brem"),
    f.Get(name+"/"+name+"_type4"),
    ]
    leg = ROOT.TLegend(0.185,0.78,0.88,0.88);
    leg.SetNColumns(2)
    leg.SetBorderSize(0);
    leg.SetFillColor(0);
    leg.SetFillStyle(0);
    leg.SetTextSize(0.045);
    hs = ROOT.THStack("hs"+name,"hs"+name)
    for j,h in enumerate(histos):
      nbins = h.GetNbinsX()
      h.SetBinContent(1,h.GetBinContent(0)+h.GetBinContent(1))
      h.SetBinContent(0,0)
      h.SetBinError(1,(h.GetBinError(0)**2+h.GetBinError(1)**2)**0.5)
      h.SetBinError(0,0)
      h.SetBinContent(nbins,h.GetBinContent(nbins+1)+h.GetBinContent(nbins))
      h.SetBinContent(nbins+1,0)
      h.SetBinError(nbins,(h.GetBinError(nbins)**2+h.GetBinError(nbins+1)**2)**0.5)
      h.SetBinError(nbins+1,0)
      leg.AddEntry(h,"#font[42]{"+typ[j]+" (%.2E)}" % h.Clone().GetSum(),"l");
      h.SetLineColor(color[j])
      h.Scale(1./h.GetSum())
      hs.Add(h)
    
    canv = ROOT.TCanvas("all","all",800,600)
    hs.Draw("hist nostack")
    hs.GetXaxis().SetTitle(name)
    hs.GetYaxis().SetTitle("Normalized Entries")
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    temp = [h.Clone() for h in histos]
    for h in temp:
      h.SetBinContent(h.GetNbinsX(),0)
    hs.SetMaximum(1.3*max([h.GetMaximum() for h in temp]))
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    leg.Draw()
    ROOT.ATLASLabel(0.2,0.89,"internal",1)
    ROOT.myText(0.5,0.89,1,"R21 single electron")
    if i==0:
      canv.Print(options.outfile+"(")
    elif i+1==len(f.GetListOfKeys()):
      canv.Print(options.outfile+")")
    else:
      canv.Print(options.outfile)





if __name__=='__main__': main()